const fs = require('fs');
const path = require('path');
const JSON_DIR_PATH = path.resolve('assets/test_json');
const SOUND_DIR_PATH = path.resolve('assets/sound');
const SOUND_REL_PATH = path.resolve('/assets/sound');
const IMG_DIR_PATH = path.resolve('assets/img');
const IMG_REL_PATH = path.resolve('/assets/img');
const APP_PATH = "https://hidden-lake-52344.herokuapp.com";

const getAllTests = (req, res, next) => {
    let allTest = {};
    fs.readdir(JSON_DIR_PATH, (err, files) => {
        if(err)
            return res.status(404).json({'status':'failed', 'data':err.message});
        let fileCount = 0;
        files.forEach((file, index, array) => {
            console.log(file);
            fs.readFile(path.join(JSON_DIR_PATH, file), (err, data) => {
                if(err)
                    return res.status(404).json({'status':'failed', 'data':err.message});
                try {
                    allTest[file] = JSON.parse(data);
                } catch(err) {
                    return res.status(404).json({'status':'failed', 'data':err.message});
                }
                fileCount++;
                if(fileCount === array.length){
                    return res.status(200).json({'status':'success', 'data':allTest});
                }    
            });
        });
    });
};

const getOneTest = (req, res, next) => {
    const  name  = req.nameFromParam;
    const fullPath = path.join(JSON_DIR_PATH, name + '.json');
    fs.readFile(fullPath, (err, data) => {
        if(err) return res.status(404).json({'status':'failed', 'data':err.message});
        res.status(200).json({'status':'success', 'data':JSON.parse(data)});
    });
};

const getTestList = (req, res, next) => {
    fs.readdir(JSON_DIR_PATH, (err, files) => {
        if(err) return res.status(404).json({'status':'failed', 'data':err.message});
        res.status(200).json({'status':'success', 'data':files});
    });
};

const getAllSounds = (req, res, next) => {
    fs.readdir(SOUND_DIR_PATH, (err, sounds) => {
        if(err) return res.status(404).json({'status':'failed', 'data':err.message});
        const soundsFullPath = sounds.map(s => path.join(APP_PATH, SOUND_REL_PATH, s ));
        res.status(200).json({'status':'success', 'data':soundsFullPath});
    });
};

const getOneSound = (req, res, next) => {
    const name = req.nameFromParam;
    fs.readdir(SOUND_DIR_PATH, (err, sounds) => {
        if(err) return res.status(404).json({'status':'failed', 'data':err.message});
        if(!sounds.includes(name)) return res.status(404).json({'status':'failed', 'data':'sound cannot find'});
        const fullPath = path.join(APP_PATH, SOUND_REL_PATH, req.nameFromParam);
        res.status(200).json({'status':'success', 'data' : fullPath});
    });

};

const getSoundList = (req, res, next) => {
    fs.readdir(SOUND_DIR_PATH, (err, sounds) => {
        if(err) return res.status(404).json({'status':'failed', 'data':err.message});
        res.status(200).json({'status':'success', 'data':sounds});
    });
};

const getAllImages = (req, res, next) => {
    fs.readdir(IMG_DIR_PATH, (err, images) => {
        if(err) return res.status(404).json({'status':'failed', 'data':err.message});
        const imagesFullPath = images.map(s => path.join(APP_PATH, IMG_REL_PATH, s ));
        res.status(200).json({'status':'success', 'data':imagesFullPath});
    });
};

const getOneImage = (req, res, next) => {
    const name = req.nameFromParam;
    fs.readdir(IMG_DIR_PATH, (err, images) => {
        if(err) return res.status(404).json({'status':'failed', 'data':err.message});
        if(!images.includes(name)) return res.status(404).json({'status':'failed', 'data':'sound cannot find'});
        const fullPath = path.join(APP_PATH, SOUND_REL_PATH, req.nameFromParam);
        res.status(200).json({'status':'success', 'data' : fullPath});
    });
};

const getImageList = (req, res, next) => {
    fs.readdir(IMG_DIR_PATH, (err, images) => {
        if(err) return res.status(404).json({'status':'failed', 'data':err.message});
        res.status(200).json({'status':'success', 'data':images});
    });
};

module.exports = {
    getAllTests,
    getOneTest,
    getTestList,
    getAllSounds,
    getOneSound,
    getSoundList,
    getAllImages,
    getOneImage,
    getImageList
}