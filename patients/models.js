const dbPool = require('../base/db');
const utils = require('../base/utils');

const getAllPatients = (callback) => {
    dbPool.query('SELECT * from patients', callback);
};

const getOnePatient = (id ,callback) => {
    dbPool.query('SELECT * from patients WHERE id = $1', [id], callback);
};

const addPatient = (patient, callback) => {
    dbPool.query('INSERT INTO patients (id, name, surname, birth_date, age, gender, file_number, phone_number, city, mother_edu_level, father_edu_level, created_on) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12) RETURNING *', Object.values(patient), callback);  
};

const deletePatient = (id, callback) => {
    dbPool.query('DELETE FROM patients WHERE id = $1 RETURNING *', [id], callback);
};

const updatePatient = (patient, callback) => {
    let queryStr = 'UPDATE patients SET';
    let fieldNO = 1;
    let valueList = [];
    if(patient.name){
        queryStr += ' name = $' + fieldNO++ + ',';
        valueList.push(patient.name);
    }
          
    if(patient.surname){
        queryStr += ' surname = $' + fieldNO++ + ',';
        valueList.push(patient.surname);
    }
          
    if(patient.birth_date){
        queryStr += ' birth_date = $' + fieldNO++ + ',';
        valueList.push(patient.birth_date);
    }
          
    if(patient.age){
        queryStr += ' age = $' + fieldNO++ + ',';
        valueList.push(patient.age);
    }
          
    if(patient.gender){
        queryStr += ' gender = $' + fieldNO++ + ',';
        valueList.push(patient.gender);
    }

    if(patient.file_number){
        queryStr += ' file_number = $' + fieldNO++ + ',';
        valueList.push(patient.file_number);
    }

    if(patient.phone_number){
        queryStr += ' phone_number = $' + fieldNO++ + ',';
        valueList.push(patient.phone_number);
    }

    if(patient.city){
        queryStr += ' city = $' + fieldNO++ + ',';
        valueList.push(patient.city);
    }

    if(patient.mother_edu_level){
        queryStr += ' mother_edu_level = $' + fieldNO++ + ',';
        valueList.push(patient.mother_edu_level);
    }
     
    if(patient.father_edu_level){
        queryStr += ' father_edu_level = $' + fieldNO++ + ',';
        valueList.push(patient.father_edu_level);
    }

    queryStr += ' WHERE id = $' + fieldNO++;
    valueList.push(patient.id);
    queryStr += ' RETURNING *';

    dbPool.query(utils.deleteLast(',', queryStr), valueList, callback);
};

module.exports = {
    getAllPatients,
    getOnePatient,
    addPatient,
    deletePatient,
    updatePatient
};