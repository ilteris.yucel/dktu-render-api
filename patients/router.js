const express = require('express');
const router = express.Router();
const controllers = require('./controllers');

router.get('/', controllers.getAllPatients);
router.get('/:id', controllers.getOnePatient);
router.post('/', controllers.addPatient);
router.delete('/:id', controllers.deletePatient);
router.put('/:id', controllers.updatePatient);

module.exports = router;