const fs = require('fs');
const path = require('path');

module.exports = {
  deleteLast : (chr, str) => {
    let finalString = '';
    let indexPosition = str.lastIndexOf(chr);
    finalString = str.substring(0, indexPosition) +
      str.substring(indexPosition+1, str.length);
    return finalString;
  },
  readDir : (dirname) => {
    return new Promise((resolve, reject) => {
      fs.readdir(dirname, (err, files) =>{
        if(err)
          reject(err);
        resolve(files);
      })
    });
  },
  readFile : (filename) => {
    return new Promise((resolve, reject) => {
      fs.readFile(filename, 'utf-8', (err, data) =>{
        if(err)
          reject(err);
        resolve(data);
      })
    });
  },
  readFiles : (fileList) => {
    return new Promise((resolve, reject) => {
      const promises = fileList.map(file => readFile(file));
      Promise.all(promises).then((data) => resolve(data)).catch(err => reject(err));
    })
  }
}
