const express = require('express');
const router = express.Router();
const controllers = require('./controllers');

router.param('result_id', (req, res, next, result_id) => {
    req.resultIDFromParam = result_id;
    next();
});

router.get('/', controllers.getAllTestResults);
router.get('/patient/:patient_id', controllers.getPatientTestResults);
router.get('/:result_id', controllers.getOneTestResult);
router.post('/', controllers.addTestResult);
router.delete('/patient/:patient_id', controllers.deletePatientTestResults);
router.delete('/:result_id', controllers.deleteTestResult);

module.exports = router;