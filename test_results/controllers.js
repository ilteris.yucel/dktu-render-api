const models = require('./models');
const { v4: uuidv4 } = require('uuid');

const getAllTestResults = (req, res, next) => {
    models.getAllTestResults((error, result) => {
        if (error)
            return res.status(406).json({'status' : 'failed', 'data' : error });
        res.status(200).json({'status' : 'success', 'data' : result.rows});
    });
};

const getOneTestResult = (req, res, next) => {
    const result_id = req.resultIDFromParam;
    if(!result_id)
        return res.status(400).json({'status' : 'failed', 'data' : 'invalid result_id' });
    models.getOneTestResult(result_id, (error, result) => {
        if(error)
            return res.status(406).json({'status' : 'failed', 'data' : error });
        res.status(200).json({'status' : 'success', 'data' : result.rows});
    });
};

const getPatientTestResults = (req, res, next) => {
    const {patient_id} = req.params;
    if(!patient_id)
        return res.status(400).json({'status' : 'failed', 'data' : 'invalid patient_id' });
    models.getPatientTestResults(patient_id, (error, result) => {
        if(error)
            return res.status(406).json({'status' : 'failed', 'data' : error });
        res.status(200).json({'status' : 'success', 'data' : result.rows});
    });
};

const addTestResult = (req, res, next) => {
    if(!(
        req.body.patient_id !== null&&
        req.body.test_name !== null&&
        req.body.result !== null&&
        req.body.is_adaptive !== null &&
        req.body.is_noisy !== null &&
        req.body.rate  !== null&&
        req.body.status !== null &&
        req.body.last_diff !== null &&
        req.body.assume_rate !== null
    )){
        console.log("required flags missing");
        return res.status(400).json({'status' : 'failed', 'data' : 'some required fields are missing'});

    }
    const test_result = {
        'result_id' : uuidv4(),
        ...req.body,
    };
    models.addTestResult(test_result, (error, result) => {
        if(error){
            console.error(error);
            return res.status(406).json({'status' : 'failed', 'data' : error });
        }
        res.status(201).json({'status' : 'success', 'data' : result.rows});            
    });
};

const deleteTestResult = (req, res, next) => {
    const result_id = req.resultIDFromParam;
    if(!result_id)
        return res.status(400).json({'status' : 'failed', 'data' : 'invalid result_id' });
    models.deleteTestResult(result_id, (error, result) => {
        if(error)
            return res.status(406).json({'status' : 'failed', 'data' : error });
        res.status(200).json({'status' : 'success', 'data' : result.rows});            
    });
};

const deletePatientTestResults = (req, res, next) => {
    const {patient_id} = req.params;
    models.deletePatientTestResults(patient_id, (error, result) => {
        if(error)
            return res.status(406).json({'status' : 'failed', 'data' : error });
        res.status(200).json({'status' : 'success', 'data' : result.rows});            
    });
};

module.exports = {
    getAllTestResults,
    getOneTestResult,
    getPatientTestResults,
    addTestResult,
    deleteTestResult,
    deletePatientTestResults
};

