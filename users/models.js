const dbPool = require('../base/db');
const utils = require('../base/utils');

const getAllUsers = (callback) => {
    dbPool.query('SELECT * from users', callback);
};

const getOneUser = (user_id ,callback) => {
    dbPool.query('SELECT * from users WHERE user_id = $1', [user_id], callback);
};

const addUser = (user, callback) => {
    dbPool.query('INSERT INTO users (username, password, email, created_on, last_login) VALUES ($1, $2, $3, $4, $5) RETURNING *', Object.values(user), callback);  
};

const updateUser = (user, callback) => {
    let queryStr = 'UPDATE users SET';
    let fieldNO = 1;
    let valueList = [];
    if(user.username){
        queryStr += ' username = $' + fieldNO++ + ',';
        valueList.push(user.username);
    }
          
    if(user.password){
        queryStr += ' password = $' + fieldNO++ + ',';
        valueList.push(user.password);
    }
          
    if(user.email){
        queryStr += ' email = $' + fieldNO++ + ',';
        valueList.push(user.email);
    }
          
    if(user.createdOn){
        queryStr += ' created_on = $' + fieldNO++ + ',';
        valueList.push(user.createdOn);
    }
          
    if(user.lastLogin){
        queryStr += ' last_login = $' + fieldNO++ + ',';
        valueList.push(user.lastLogin);
    }
          
    queryStr += ' WHERE user_id = $' + fieldNO++;
    valueList.push(user.user_id);
    queryStr += ' RETURNING *';

    dbPool.query(utils.deleteLast(',', queryStr), valueList, callback);
};

const deleteUser = (user_id, callback) => {
    dbPool.query('DELETE FROM users WHERE user_id=$1 RETURNING *', [user_id], callback);
};

const signInUser = (user, callback) => {
    dbPool.query('SELECT password FROM users WHERE username=$1 OR email=$1', [user] ,callback);
}

module.exports = {
    getAllUsers,
    getOneUser,
    addUser,
    updateUser,
    deleteUser,
    signInUser,
};