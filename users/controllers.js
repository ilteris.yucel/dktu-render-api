const models = require('./models');

const getAllUsers = (req, res, next) => {
    models.getAllUsers((error, result) => {
        if(error)
            return res.status(406).json({'status' : 'failed', 'data' : error });
        res.status(200).json({'status' : 'success', 'data' : result.rows});
    });
};

const getOneUser = (req, res, next) => {
    const { user_id } = req.params;
    if(!user_id)
        return res.status(400).json({'status' : 'failed', 'data' : 'invalid user_id' });
    models.getOneUser(user_id, (error, result) => {
        if(error)
            return res.status(406).json({'status' : 'failed', 'data' : error });
        res.status(200).json({'status' : 'success', 'data' : result.rows});
    });
};

const addUser = (req, res, next) => {
    // const { username, password, email } = req.body;
    const createdOn = new Date();
    if(!(req.body.username && req.body.password && req.body.email))
        return res.status(400).json({'status' : 'failed', 'data' : 'username, password or email cannot be empty' });
    const user = {
        ...req.body,
        'created_on': createdOn,
        'last_login': null,
    };
    models.addUser(user, (error, result) => {
        if(error)
            return res.status(406).json({'status' : 'failed', 'data' : error });
        res.status(201).json({'status' : 'success', 'data' : result.rows});        
    });
};

const updateUser = (req, res, next) => {
    const { user_id } = req.params; 
    if(!user_id)
        return res.status(400).json({'status' : 'failed', 'data' : 'invalid user_id' });
    const user = {
        'user_id': user_id,
        ...req.body,
    };
    models.updateUser(user, (error, result) => {
        if(error)
            return res.status(406).json({'status' : 'failed', 'data' : error });
        res.status(200).json({'status' : 'success', 'data' : result.rows});  
    });
};

const deleteUser = (req, res, next) => {
    const { user_id } = req.params; 
    if(!user_id)
        return res.status(400).json({'status' : 'failed', 'data' : 'invalid user_id' });
    models.deleteUser(user_id, (error, result) => {
        if(error)
            return res.status(406).json({'status' : 'failed', 'data' : error });
        res.status(200).json({'status' : 'success', 'data' : result.rows});  
    });
};

const signInUser = (req, res, next) => {
    const { user, password } = req.body;
    if(!(user && password))
        return res.status(400).json({'status' : 'failed', 'data' : 'username and password are required' });
    models.signInUser(user, (error, result) => {
        if(error)
            return res.status(406).json({'status' : 'failed', 'data' : error });
        const status = result.rows
            && result.rows[0] 
            && result.rows[0]['password'] 
            && result.rows[0]['password'] === password;
            if(status)
                return res.status(200).json({'status' : 'success', "data" : 'sing in is successful'});
            else
                return res.status(400).json({'status' : 'success', "data" : 'sing in is not successful'});
    });    

};

module.exports = {
    getAllUsers,
    getOneUser,
    addUser,
    updateUser,
    deleteUser,
    signInUser,
};