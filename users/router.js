const express = require('express');
const router = express.Router();
const controllers = require('./controllers');

router.get('/', controllers.getAllUsers);
router.get('/:user_id', controllers.getOneUser);
router.post('/', controllers.addUser);
router.delete('/:user_id', controllers.deleteUser);
router.put('/:user_id', controllers.updateUser);
router.post('/signin', controllers.signInUser);

module.exports = router;